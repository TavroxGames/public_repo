﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TavroxLib
{
    public static class PlayerPrefers
    {
        public static bool GetBool(string _key)
        {
            int _val = PlayerPrefs.GetInt(_key);
            if (_val == 1) { return true; }
            else { return false; }
        }
        public static void SetBool(string _key,  bool _do)
        {
            if (_do)
            {
                PlayerPrefs.SetInt(_key, 1); // true
            }
            else
            {
                PlayerPrefs.SetInt(_key, 0); // false
            }
        }

    }
}