﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TavroxLib
{
    public static class TransformExtension
    {
        public static void pos2D(this Transform _t, float _x, float _y)
        {
            _t.position = new Vector3(_x, _y, 0f);
        }
    }
}
