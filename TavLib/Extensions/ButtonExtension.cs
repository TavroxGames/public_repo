﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TavroxLib
{
    public static class ButtonExtension
    {

        public static void setText(this Button _b, string _t)
        {
            _b.GetComponentInChildren<TextMeshProUGUI>().text = _t;
        }
        public static string getText(this Button _b)
        {
            return(_b.GetComponentInChildren<TextMeshProUGUI>().text);
        }

        public static void setColor(this Button _b, Color _col)
        {
            _b.GetComponent<Image>().color = _col;
        }

        public static void Disable(this Button _b)
        {
            _b.interactable = false;
            _b.GetComponent<Image>().color = Color.clear ;
        }

        public static void Enable(this Button _b)
        {
            _b.interactable = true;
            _b.GetComponent<Image>().color = Color.white;
        }
    }
}
