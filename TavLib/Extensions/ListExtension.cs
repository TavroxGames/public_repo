﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TavroxLib
{
    public static class ListExtension
    {
        // Attention qu'IndexOf peut renvoyer -1 si NotFound
        //pour faire du previous ou next, les liste doublement chainées
        //https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.linkedlist-1?view=netframework-4.8
        //Si tu ne l'utilises qu'en éditeur ou à faible fréquence sur de petits ensembles, tu peux. Mais je te suggère éventuellement de faire des variantes avec l'index plutôt que l'objet


        public static T Previous<T>(this IList<T> _li, T _obj)
        {
            int _prev = _li.IndexOf(_obj) - 1;
            if (_prev == -1) { return _li[_li.Count-1]; }
            else return _li[_prev];
        }
        
        public static T Next<T>(this IList<T> _li, T _obj)
        {
            int _next = _li.IndexOf(_obj) + 1;
            if (_next == _li.Count) { return _li[0]; }
            else return _li[_next];
        }

        public static IList<T> Shuffle<T>(this IList<T> _li)
        {
            var count = _li.Count;
            var last = count - 1;
            for (var i = 0; i < last; ++i)
            {
                var r = UnityEngine.Random.Range(i, count);
                var tmp = _li[i];
                _li[i] = _li[r];
                _li[r] = tmp;
            }
            return _li;
        }
        public static List<T> CopyToList<T>(this IList<T> _li) 
        {
            T[] _arr = new T[_li.Count];
            _li.CopyTo(_arr, 0);
            return(_arr.ToList());
        }

        public static string ToDebug<T>(this T[] _li)
        {
            string _deb = "Count:" + _li.Length + System.Environment.NewLine;
            foreach (var item in _li)
            {
                _deb += item.ToString() + System.Environment.NewLine;
            }
            return _deb;
        }
        public static string ToDebug<T>(this IList<T> _li)
        {
            if (_li == null) return "Null List";
            string _deb = "Count:"+_li.Count + System.Environment.NewLine ;
            foreach (var item in _li)
            {
                _deb += item.ToString() + System.Environment.NewLine;
            }
            return _deb;
        }

        public static T GetRandom<T>(this IList<T> _li)
        {
            int _rand = Random.Range(0, _li.Count);
            if (_li.Count <= 0) { Debug.LogError(_li + " doesn't have enough items. [>" + _li.Count); }
            if (_li[_rand] == null) { Debug.LogError("Couldn't find item at " + _rand); }
            return _li[ _rand];
        }

        public static T GetRandomExcept<T>(this IList<T> _li, T _except)
        {
            _li.Remove(_except);
            int _rand = Random.Range(0, _li.Count);
            if (_li.Count <= 0) { Debug.LogError(_li + " doesn't have enough items. [>" + _li.Count); }
            if (_li[_rand] == null) { Debug.LogError("Couldn't find item at " + _rand); }
            return _li[_rand];
        }
    }
}