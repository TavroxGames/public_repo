﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TavroxLib
{
    public static class CanvasGroupExtension
    {

        public static void Hide(this CanvasGroup _canvas, bool _fade = false, float _duration = .1f)
        {
            if (_canvas == null) { Debug.LogError("No Canvas Group added to object"); }
            if (_fade)
            {
                _canvas.DOFade(0f, _duration);
            }
            else
            {
                _canvas.alpha = 0f;
            }
            _canvas.blocksRaycasts = false;
            _canvas.interactable = false;
        }

        public static void Show(this CanvasGroup _canvas, bool _fade = false, float _duration = .1f)
        {
            if (_canvas == null) { Debug.LogError("No Canvas Group added to object"); }
            if (_fade)
            {
                _canvas.DOFade(1f, _duration);
            }
            else
            {
                _canvas.alpha = 1f;
            }
            _canvas.blocksRaycasts = true;
            _canvas.interactable = true;
        }

        public static void disabRaycasts(this CanvasGroup _canvas)
        {
            _canvas.blocksRaycasts = false;
            _canvas.interactable = false;
        }

        public static void enabRaycasts(this CanvasGroup _canvas)
        {
            _canvas.blocksRaycasts = true;
            _canvas.interactable = true;
        }
    }
}
