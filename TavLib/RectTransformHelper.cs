﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TavroxLib
{
    public enum Alignment
    {
        Middle = 0,
        Left = 1,
        Top = 2,
        Right = 3
    };

    public class RectTransformHelper : MonoBehaviour
    {
        public enum EdgeCases
        {
            None = 0,
            OverflowLeft = 1,
            OverflowRight = 2,
        };


        internal void placeAutoExplanation(RectTransform _targetWindow, RectTransform _originPoint, float _padX = 30f, float _padY = 0, Alignment _alignCase = Alignment.Left)
        {
            EdgeCases _edge = EdgeCases.None;
            Rect _rec = RectTransformToScreenSpace(_targetWindow);
            // For now we only have Left / Right problem managed, maybe should add top / down in the future.

            // Check edge cases
            if ((_originPoint.position.x + _rec.size.x) > Screen.width) // |OOOO|X
            {
                _edge = EdgeCases.OverflowRight;
                _alignCase = Alignment.Left;
            }
            else if ((_originPoint.position.x - _rec.size.x) < 0) // X|OOOOO|
            {
                _edge = EdgeCases.OverflowLeft;
                _alignCase = Alignment.Right;
            }         

            // Position according to align case
            switch (_alignCase)
            {
                case Alignment.Middle:
                    break;
                case Alignment.Left:
                    _targetWindow.position = new Vector2(_originPoint.position.x - _rec.size.x, _originPoint.position.y);
                    break;
                case Alignment.Top:
                    break;
                case Alignment.Right:
                    _targetWindow.position = new Vector2(_originPoint.position.x + _rec.size.x, _originPoint.position.y);
                    break;
                default:
                    break;
            }


        }

        public static Rect RectTransformToScreenSpace(RectTransform transform)
        {
            Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
            return new Rect((Vector2)transform.position - (size * 0.5f), size);
        }

        public Vector3 worldToUISpace(Canvas parentCanvas, Vector3 worldPos)
        {
            //Convert the world for screen point so that it can be used with ScreenPointToLocalPointInRectangle function
            Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
            Vector2 movePos;

            //Convert the screenpoint to ui rectangle local point
            RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out movePos);
            //Convert the local point to world point
            return parentCanvas.transform.TransformPoint(movePos);
        }

        // ANCHOR MUST BE IN THE CENTER!!
        // Careful if one day you can resize game screen resolution at will. 

        //It works when target is INSIDE the model
        internal void placeAutoInside(RectTransform _targ, RectTransform _modl, float _padX = 30f, float _padY = 0, Alignment _alignCase = Alignment.Left)
        {
            EdgeCases _edge = EdgeCases.None;
            // For now we only have Left / Right problem managed, maybe should add top / down in the future.

            if ((_targ.position.x + _targ.sizeDelta.x) > Screen.width) // |OOOO|X
            {
                _edge = EdgeCases.OverflowRight;
            }
            else if ((_targ.position.x + _targ.sizeDelta.x) < 0 + _targ.sizeDelta.x) // X|OOOOO|
            {
                _edge = EdgeCases.OverflowLeft;
            }

            if (_edge != EdgeCases.None)
            {
                switch (_edge)
                {
                    case EdgeCases.OverflowLeft:
                        _alignCase = Alignment.Right;
                        break;
                    case EdgeCases.OverflowRight:
                        _alignCase = Alignment.Left;
                        break;
                }
            }

            switch (_alignCase)
            {
                case Alignment.Middle:
                    break;
                case Alignment.Left:
                    _targ.anchoredPosition = new Vector2(-_modl.sizeDelta.x - _padX, 0f + _padY);
                    break;
                case Alignment.Top:
                    break;
                case Alignment.Right:
                    _targ.anchoredPosition = new Vector2(_modl.sizeDelta.x + _padX, 0f + _padY);
                    break;
                default:
                    break;
            }

        }


        internal void placeAutoOutside(RectTransform _targ, RectTransform _modl, float _padX = 30f, float _padY = 0f)
        {
            //_targ.anchoredPosition = new Vector2(_modl.sizeDelta.x + 10f, 0f);
            // For now we only have Left / Right problem managed, maybe should add top / down in the future.
            if ((_targ.position.x + _targ.sizeDelta.x) > Screen.width) // |OOOO|X
            {
                _targ.anchoredPosition = new Vector2(_targ.transform.position.x + _modl.sizeDelta.x - _padX, _targ.transform.position.y + 0f + _padY);
            }
            else if ((_targ.position.x + _targ.sizeDelta.x) < 0 + _targ.sizeDelta.x) // X|OOOOO|
            {
                _targ.anchoredPosition = new Vector2(_targ.transform.position.x + _modl.sizeDelta.x + _padX, _targ.transform.position.y + 0f + _padY);
            }
        }

        // TODO
        internal void horizontalLayoutAuto(List<RectTransform> _targets, RectTransform _model, TextAnchor _layoutMode)
        {
            switch (_layoutMode)
            {
                case TextAnchor.UpperLeft:
                    break;
                case TextAnchor.UpperCenter:
                    break;
                case TextAnchor.UpperRight:
                    break;
                case TextAnchor.MiddleLeft:
                    break;
                case TextAnchor.MiddleCenter:
                    break;
                case TextAnchor.MiddleRight:
                    break;
                case TextAnchor.LowerLeft:
                    break;
                case TextAnchor.LowerCenter:
                    break;
                case TextAnchor.LowerRight:
                    break;
                default:
                    break;
            }
        }
    }

}