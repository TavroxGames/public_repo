﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace TavroxLib
{
    [CustomEditor(typeof(CanvasGroup))]
    public class CanvasGroupEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            EditorGUILayout.LabelField("Tavrox Lib");
            CanvasGroup targ = (CanvasGroup)target;
            if (GUILayout.Button("Show"))
            {
                targ.Show();
                Undo.RecordObject(targ, "Show");
            }
            if (GUILayout.Button("Hide"))
            {
                targ.Hide();
                Undo.RecordObject(targ, "Hide");
            }
        }
    }

}