﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public class BetterToggleGroup : ToggleGroup
{
    public delegate void ChangedEventHandler(Toggle newActive);

    public event ChangedEventHandler OnChange;

    // Must be called after instanciation
    internal void Setup(Toggle _target)
    {
        _target.group = this;
        _target.onValueChanged.AddListener((isSelected) => {
            if (!isSelected)
            {
                return;
            }
            var activeToggle = Active();
            DoOnChange(activeToggle);
        });
        // NB : DoOnChange is always triggered at setup. Couldn't do otherwise. Dunno why.
    }

    internal void Setup(List<Toggle> _togs)
    {
        foreach (Toggle _tg in _togs)
        {
            Setup(_tg);
        }
    }

    public Toggle Active()
    {
        return ActiveToggles().FirstOrDefault();
    }

    protected virtual void DoOnChange(Toggle newactive)
    {
        var handler = OnChange;
        if (handler != null) handler(newactive);
    }
}