﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace TavroxLib
{
    public class PicturedLine : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler
    {
        TextMeshProUGUI Txt;
        Image Pic;
        internal bool Used;
        bool isSet;

        UnityAction Hover, Click;

        internal void Setup()
        {
            Pic = transform.Find("Img").GetComponent<Image>();
            Txt = transform.Find("Txt").GetComponent<TextMeshProUGUI>();
            isSet = true;
            Used = false;
        }

        internal void setupActions(UnityAction _hover, UnityAction _click)
        {
            Hover += _hover;
            Click += _click;
        }

        internal void Fill(Sprite _pic, string _txt)
        {
            if (!isSet) Debug.LogError("Must setup pictured line first", this);
            Used = true;
            Pic.sprite = _pic;
            Txt.text = _txt;
        }

        internal void Clear()
        {
            if (!isSet) Debug.LogError("Must setup pictured line first", this);
            Used = false;
            Pic.sprite = null;
            Txt.text = "Empty";
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (Click != null)
                Click.Invoke();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (Hover != null)
                Hover.Invoke();
        }
    }

}