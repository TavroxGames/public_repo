﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TavroxLib;
using UnityEngine.Events;

namespace TavroxLib
{
    public class UIFX : MonoBehaviour
    {
        RectTransform Trans;
        Vector2 initPos, botPos, topPos;
        bool Playing;

        internal CanvasGroup Can;

        public float fxSpeed = 0.3f;
        public float factorOffset = 0.2f;

        private void Awake()
        {
            Trans = GetComponent<RectTransform>();
            Can = GetComponent<CanvasGroup>();
            if (Can == null) { Can = transform.GetChild(0).GetComponent<CanvasGroup>(); }
            if (Can == null) { Can = gameObject.AddComponent<CanvasGroup>(); }
            initPos = Trans.anchoredPosition;
            botPos = new Vector2(Trans.anchoredPosition.x, Trans.anchoredPosition.y - (Trans.sizeDelta.y * factorOffset));
            topPos = new Vector2(Trans.anchoredPosition.x, Trans.anchoredPosition.y + (Trans.sizeDelta.y * factorOffset));
            //TODO : Modify bot pos & top pos according to size of parent! Else, smaller canvas have ood 
        }

        internal void Appear()
        {
            DOTween.To(() => Can.alpha, x => Can.alpha = x, 1f, fxSpeed).OnComplete(() => endAction(() => Can.Show()));
        }

        internal void Disappear()
        {
            DOTween.To(() => Can.alpha, x => Can.alpha = x, 0f, fxSpeed).OnComplete(() => endAction(() => Can.Hide()));
        }

        internal void appearFromBottom()
        {
            if (Playing)
                return;

            Playing = true;
            Trans.anchoredPosition = botPos;
            DOTween.To(() => Can.alpha, x => Can.alpha = x, 1f, fxSpeed).OnComplete(() => endAction(() => Can.Show()));
            Trans.DOAnchorPos(initPos, fxSpeed);
        }

        internal void DisappearSlideDown()
        {
            if (Playing)
                return;

            Playing = true;
            Trans.anchoredPosition = botPos;
            DOTween.To(() => Can.alpha, x => Can.alpha = x, 0f, fxSpeed).OnComplete(() => endAction(() => Can.Hide()));
            Trans.DOAnchorPos(botPos, fxSpeed);
        }

        internal void DisappearSlideDown(UnityAction _act)
        {
            if (Playing)
                return;
            DOTween.To(() => Can.alpha, x => Can.alpha = x, 0f, fxSpeed).OnComplete(() => endAction(_act));
            Trans.DOAnchorPos(botPos, fxSpeed);
        }

        internal void DisappearSlideTop(UnityAction _act)
        {
            if (Playing)
                return;
            DOTween.To(() => Can.alpha, x => Can.alpha = x, 0f, fxSpeed).OnComplete(() => endAction(_act));
            Trans.DOAnchorPos(topPos, fxSpeed);
        }

        internal void appearPunch()
        {
            Trans.DOPunchScale(new Vector3(0.1f, 0.1f, 0f), 0.3f);
        }

        void endAction(UnityAction _act = null)
        {
            Playing = false;
            _act.Invoke();
        }
    }
}