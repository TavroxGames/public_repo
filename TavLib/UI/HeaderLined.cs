﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HeaderLined : MonoBehaviour
{
    TextMeshProUGUI Txt, Header;
    internal bool isUsed;

    public HeaderLined Setup()
    {
        Header = transform.Find("Header").GetComponent<TextMeshProUGUI>();
        Txt = transform.Find("Txt").GetComponent<TextMeshProUGUI>();
        Hide();
        return this;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void setText(string _head, string _t)
    {
        Show();
        isUsed = true;
        Txt.text = _t;
        Header.text = _head;
    }

    public void Clear()
    {
        isUsed = false;
        Txt.text = "";
        Header.text = "";
        Hide();
    }
}
