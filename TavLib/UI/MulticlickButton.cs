﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TavroxLib
{
    public class MulticlickButton : Button, IPointerClickHandler
    {

        UnityAction rightClick;
        UnityAction leftClick;

        internal void Setup(UnityAction _left, UnityAction _right)
        {
            rightClick = _right;
            leftClick = _left;
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Right)
            {
                rightClick.Invoke();
            }

            if (eventData.button == PointerEventData.InputButton.Left)
            {
                leftClick.Invoke();
            }
        }
    }

}