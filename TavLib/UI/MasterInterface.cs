﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasterInterface : SerializedMonoBehaviour
{
    public void getInterfClass<T>(ref T _obj) where T : Component
    {
        //Debug.Log(_obj, _obj);
        if (_obj != null) { Debug.Log("Already there:" + _obj); return; }
        _obj = GetComponentInChildren<T>();
        _obj.gameObject.SetActive(false);
        //Debug.Log(_obj, _obj);
    }

    public void getInterfName<T>(ref T _obj, string _find) where T : Component
    {
        //Debug.Log(_obj, _obj);
        if (_obj != null) { Debug.Log("Already there:" + _obj); return; }
        _obj = transform.Find(_find).GetComponent<T>();
        _obj.gameObject.SetActive(false);
        //Debug.Log(_obj, _obj);
    }
}
