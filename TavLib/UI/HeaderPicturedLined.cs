﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TavroxLib
{
    public class HeaderPicturedLined : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler
    {
        TextMeshProUGUI Header, Desc;
        Image Pic;
        internal bool Used;
        bool isSet;

        UnityAction Hover, Click;

        internal void Setup()
        {
            Pic = transform.Find("Img").GetComponent<Image>();
            Header = transform.Find("Header").GetComponent<TextMeshProUGUI>();
            Desc = transform.Find("Desc").GetComponent<TextMeshProUGUI>();
            isSet = true;
            Used = false;
        }

        internal void setupActions(UnityAction _hover, UnityAction _click)
        {
            Hover += _hover;
            Click += _click;
        }

        internal void Fill(Sprite _pic, string _header, string _desc)
        {
            if (!isSet) Debug.LogError("Must setup pictured line first", this);
            Used = true;
            Pic.sprite = _pic;
            Header.text = _header;
            Desc.text = _desc;
        }

        internal void Clear()
        {
            if (!isSet) Debug.LogError("Must setup pictured line first", this);
            Used = false;
            Pic.sprite = null;
            Header.text = "[HEADER]";
            Desc.text = "[DESC]";
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (Click != null)
                Click.Invoke();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (Hover != null)
                Hover.Invoke();
        }
    }

}