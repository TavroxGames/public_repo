﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class HoverButton : Button, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    Image Img;
    RectTransform My;

    protected override void Start()
    {
        base.Start();
        My = GetComponent<RectTransform>();
        Img = GetComponent<Image>();
        StartCoroutine(lateStart());
    }

    IEnumerator lateStart()
    {
        yield return new WaitForSeconds(1f);
    }
    
    public override void OnPointerEnter(PointerEventData eventData)
    {
        My.DOScale(new Vector3(1.3f, 1.3f, 1f), 0.3f);
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        My.DOScale(Vector3.one, 0.3f);
    }

    public override void OnPointerClick(PointerEventData eventData)
    {
        base.onClick.Invoke();
    }
}
