﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TavroxLib;
using UnityEngine;

namespace TavroxLib
{
    public class BeanBagList<T> : List<T>
    {
        List<T> Originals = new List<T>();

        public BeanBagList(List<T> _objs)
        {
            foreach (var item in _objs)
            {
                Add(item);
                Originals.Add(item);
            }
        }

        // You want to get a random but
        public T getBaggedRandom()
        {
            Debugger();
            if (Count == 0) {
                ResetBucket();
            }

            T _obj = this.GetRandom();
            if (!Contains(_obj)) { Debug.LogError("Error, wasn't it bucket!"); }
            Remove(_obj);
            return (_obj);
        }

        // You want to exclude from bucket BUT only in a certain List
        public BeanBagList<T> getRandomInclude(List<T> _toInclude)
        {
            Debugger();
            foreach (T _items in _toInclude)
            {
                Remove(_items);
            }
            return this;
        }

        void ResetBucket()
        {
            Debug.Log("reset");
            Debugger();
            foreach (var item in Originals)
            {
                Add(item);
            }
        }

        void Debugger()
        {
            // Debug.Log("BagState :" + Environment.NewLine + Environment.NewLine + this.ToDebug());
        }
    }
}